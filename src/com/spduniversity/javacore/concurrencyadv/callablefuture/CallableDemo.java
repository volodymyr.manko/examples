package com.spduniversity.javacore.concurrencyadv.callablefuture;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class CallableDemo {

    public static void main(String[] args) {
        ExecutorService executorService = Executors.newFixedThreadPool(3);
        Future<Integer> sumResult;
        Future<Double> hypotResult;
        Future<Integer> factorialResult;

        System.out.println("Start processing...");

        sumResult = executorService.submit(new Sum(10));
        hypotResult = executorService.submit(new Hypot(1, 2));
        factorialResult = executorService.submit(new Factorial(5));

        try {
            System.out.println("Hypot: " + hypotResult.get());
            System.out.println("Summa: " + sumResult.get());
            System.out.println("Factorial: " + factorialResult.get());
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        executorService.shutdown();
        System.out.println("Stop processing...");
    }
}
