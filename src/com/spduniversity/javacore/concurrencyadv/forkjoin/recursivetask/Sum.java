package com.spduniversity.javacore.concurrencyadv.forkjoin.recursivetask;

import java.util.Arrays;
import java.util.concurrent.RecursiveTask;

class Sum extends RecursiveTask<Double> {
    private final int seqThreshold = 5000;
    private final static int countProcessors = Runtime.getRuntime().availableProcessors();
    private double[] data;
    private int start;
    private int end;

    Sum(double[] data, int start, int end) {
        this.data = data;
        this.start = start;
        this.end = end;
    }

    @Override
    protected Double compute() {
        double sum = 0;

        if ((countProcessors == 1) || (end - start) < seqThreshold) {
            System.out.println("Running: start " + start + " end " + end);
            for (int i = start; i < end; i++) {
                sum += data[i];
            }
        }
        else {
            int middle = (start + end) / 2;

            Sum subTask1 = new Sum(data, start, middle);
            Sum subTask2 = new Sum(data, middle, end);

            // Variant 1
            subTask1.fork();
            subTask2.fork();
            sum = subTask1.join() + subTask2.join();


            // Variant 2
//            subTask1.fork();
//            sum = subTask1.join() + subTask2.invoke();

            // Variant 3
//            subTask1.fork();
//            sum = subTask1.join() + subTask2.compute();
        }

        return sum;
    }
}
