package com.spduniversity.javacore.objects;

public class ObjectDemo {
    public static void main(String[] args) {
        Student alex = new Student("Alex", 28);
        Student billy = new Student("Billy", 28);

        System.out.println(alex.getClass());
        System.out.println(String.format("%h", alex.hashCode()));
        System.out.println(alex.toString());
        System.out.println(alex == billy);  // false
        System.out.println(alex.equals(billy)); // false

        billy.setName("Alex");
        System.out.println(alex == billy);  // false
        System.out.println(alex.equals(billy)); // true - если мы переопределим equals

    }
}
