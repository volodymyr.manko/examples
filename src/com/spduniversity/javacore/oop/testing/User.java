package com.spduniversity.javacore.oop.testing;

public class User {
    private String name;
    private int age;
    private String login;

    public void passTest() {
        System.out.println(login + " start testing...");
    }

    public void rateTest() {
        System.out.println(login + " have rated the test.");
    }

    public void postReview() {
        System.out.println(login + " post a review.");
    }
}
