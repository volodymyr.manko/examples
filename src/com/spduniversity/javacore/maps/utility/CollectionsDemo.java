package com.spduniversity.javacore.maps.utility;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class CollectionsDemo {
    public static void main(String[] args) {
        // boolean addAll(Collection<? super T> c, T... elements);
        List<String> animalList = new ArrayList<>();
        animalList.add("Fox");
        animalList.add("Wolf");
        animalList.add("Bear");
        animalList.add(("Wolf"));

        String[] array = {"Rat"};
        Collections.addAll(animalList, array);
        System.out.println("animalList: " + animalList);

        animalList.addAll(Arrays.asList(array));

        // static <T extends Comparable<? super T>> void sort(List<T> animalList)
        Collections.sort(animalList);
        System.out.println("Sorted animalList: " + animalList);

        // <T> int binarySearch(List<? extends Comparable<? super T>> animalList, T key)
        System.out.println("Is Wolf There? : " + Collections.binarySearch(animalList, "Wolf"));

        Collections.reverse(animalList);
        System.out.println("Reverse animalList: " + animalList);

        Collections.swap(animalList, 0, 5);
        System.out.println("After swapping: " + animalList);

        System.out.println("# Wolf: " + Collections.frequency(animalList, "Wolf"));

        Collections.shuffle(animalList);
        System.out.println("Shuffled animalList: " + animalList);

        System.out.println("Max: " + Collections.max(animalList)); // natural ordering
        System.out.println("Min: " + Collections.min(animalList));
    }
}
