package com.spduniversity.javacore.objects.singleton.v3;

class Singleton {
    private Singleton() {}

    private static class SingletonHolder {
        private final static Singleton instance = new Singleton();
    }

    // Решили проблему отложенной инициализации
    public static Singleton getInstance() {
        return SingletonHolder.instance;
    }
}
