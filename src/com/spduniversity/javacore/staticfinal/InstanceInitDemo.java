package com.spduniversity.javacore.staticfinal;

public class InstanceInitDemo {
    private String name;
    static int age;

    static void staticMethod() {
        age = 10;
        System.out.println("Static method " + age);
    }

    InstanceInitDemo() {
        System.out.println("Constructor without args");
    }

    {
        System.out.println("Instance init block 1");
        staticMethod();
    }

    InstanceInitDemo(String name) {
        this.name = name;
        System.out.println("Constructor with 1 argument");
    }

    {
        System.out.println("Instance init block 2");
        age = 20;
    }

    public static void main(String[] args) {
        InstanceInitDemo demo = new InstanceInitDemo();
        InstanceInitDemo demo1 = new InstanceInitDemo("Hi");
    }
}
