package com.spduniversity.javacore.generics.wildcards.interfaces;

interface MinMax<T extends Comparable<T>> {
    T min();
    T max();
}
