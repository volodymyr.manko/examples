package com.spduniversity.javacore.abstractintefaces.logger;

import com.spduniversity.javacore.abstractintefaces.logger.exception.MyCheckedArifmeticException;
import com.spduniversity.javacore.abstractintefaces.logger.exception.MyCheckedException;
import com.spduniversity.javacore.abstractintefaces.logger.exception.MyUncheckedException;

public class TestLogger {
	static ILogger logger = new Logger();
	
	public static void main(String[] args) throws Exception {
		try{
			methodWithException();
		} catch (MyUncheckedException e) {
			System.out.println("My Exception");
		} catch (MyCheckedArifmeticException e) {
			System.out.println("My Checked Exception " + e.getMessage());
			System.out.println("a: " + e.getA() + " b: " + e.getB() );
			e.printStackTrace();
//		} catch (RuntimeException e) {
////			System.out.println(e.getMessage());
		} catch (Exception e) {
			System.out.println("Checked exceptions: " + e.getMessage());
		} finally {
			
		}
	}

	private static void methodWithException() 
			throws MyCheckedException, MyUncheckedException, Exception  {
		int a = 10;
		int b = 0;
		try {
			System.out.println(a / b);
			
			logger.log("" + Math.random());
			if(true)
				throw new Exception("Our exception");
			return;
		} catch (ArithmeticException e) {
			MyCheckedArifmeticException myCheckedArifmeticException 
					= new MyCheckedArifmeticException();
			myCheckedArifmeticException.setA(a);
			myCheckedArifmeticException.setB(b);
			myCheckedArifmeticException.setMessage(e.getMessage());
			
			throw myCheckedArifmeticException;
		} catch (Exception e) {
			logger.error(e);
			throw new MyUncheckedException("My Runtime Exception", e);
		} finally {
			System.out.println("Finally block");
//			throw new RuntimeException("Our exception");
		}
	}
}
