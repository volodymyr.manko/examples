package com.spduniversity.javacore.concurrency.examples.executors;

import java.util.concurrent.*;

public class CallableDemo {

    public static void main(String[] args) {

        ExecutorService executor = Executors.newFixedThreadPool(3);

        Callable<Integer> task = () -> {
            TimeUnit.SECONDS.sleep(3);
            return 123;
        };

        // But we can't get result
        executor.submit(task);

        // Problem with return type
        // Integer result = executor.submit(task);
    }
}
