package com.spduniversity.javacore.enums.hardcore;

import java.util.EnumSet;

public class EnumSets {
    public static void main(String[] args) {
        EnumSet<AlarmPoints> points = EnumSet.noneOf(AlarmPoints.class);
        System.out.println(points); // Empty

        points.add(AlarmPoints.Bathroom);
        System.out.println(points);

        points.addAll(EnumSet.of(AlarmPoints.Stair1, AlarmPoints.Stair2, AlarmPoints.Kitchen));
        System.out.println(points);

        points = EnumSet.allOf(AlarmPoints.class);
        System.out.println(points);

        points.removeAll(EnumSet.range(AlarmPoints.Office1, AlarmPoints.Office4));
        System.out.println(points);
    }
}
