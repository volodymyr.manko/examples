package com.spduniversity.javacore.concurrency;

public class ThreadDemoAlternative extends Thread {

    public static void main(String[] args) {
        ThreadDemoAlternative thread = new ThreadDemoAlternative();
        thread.start();

        System.out.println("Inside main() method..." + thread.getName());
    }

    @Override
    public void run() {
        System.out.println("Inside run()..." + Thread.currentThread().getName());
        methodInsideRun();
    }

    private void methodInsideRun() {
        System.out.println("Inside methodInsideRun()...");
        anotherDeepMethod();
    }

    private void anotherDeepMethod() {
        System.out.println("Inside anotherDeepMethod()...");
    }
}
