package com.spduniversity.javacore.abstractintefaces.pet;

import java.util.ArrayList;
import java.util.List;

public class Main {
	public static void main(String[] args) {
		Friend friend = new Friend();
		
		friend.setFriend(new Dog("dog"));
		friend.voice();

		friend.setFriend(new Cat("cat"));
		friend.voice();

		friend.setFriend(new Human("Human"));
		friend.voice();
		
		/*
		List<Pet> pets = pets();
		
		for (Pet pet : pets) {
			pet.barking();
		}
		/*
		
		List<Animal> animals = new ArrayList<>();
		animals.addAll(pets);
		animals.add(new Human("Igor"));
		for (Animal animal : animals) {
			animal.voice();
		}
		*/
//		Pet pet = new Pet();
	}

	private static List<Pet> pets() {
		List<Pet> pets = new ArrayList<>();
		pets.add(new Cat("Матроскин"));
		pets.add(new Cat("Vasya"));
		pets.add(new Dog("Reks"));
		return pets;
	}
}
