package com.spduniversity.javacore.io.file;

import java.io.File;
import java.io.FilenameFilter;
import java.util.Arrays;

public class FileFilterDemo {
    public static void main(String[] args) {
        File file = new File("/");
        FilenameFilter jpgFilter = new OnlyExt("pdf");
        String images[] = file.list(jpgFilter);

        Arrays.asList(images).forEach(System.out::println);
    }
}
