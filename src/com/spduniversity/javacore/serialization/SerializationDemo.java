package com.spduniversity.javacore.serialization;

import java.io.*;

public class SerializationDemo {
    private Student student;

    public static void main(String[] args) {
        if (args.length > 0 && args[0].equals("init")) {
            new SerializationDemo().doSerialization();
        }
        new SerializationDemo().doDeserialization();
    }

    private void doSerialization() {
        student = new Student("Alex", 30);
        System.out.println("Serialization ...");
        System.out.println(student);

        try (ObjectOutputStream out = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream("student.ser")))) {
            out.writeObject(student);
            out.writeObject("Hello");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void doDeserialization() {
        System.out.println("Deserialization....");

        try (ObjectInputStream in = new ObjectInputStream(new BufferedInputStream(new FileInputStream("student.ser")))) {
            String myStr = (String)in.readObject();
            System.out.println(myStr);
            student = (Student) in.readObject();
            System.out.println(student);
        } catch (ClassNotFoundException | IOException e) {
            e.printStackTrace();
        }
    }
}
