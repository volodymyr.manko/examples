package com.spduniversity.javacore.strings;

public class StringPoolDemo {
    public static void main(String[] args) {
        String s1 = "Hello";
        String s2 = "Hello";
        String s3 = "Hello".intern();
        String s4 = new String("Hello");
        String s5 = "llo";
        final String s6 = "llo";

        System.out.println("s1 == s2: " + (s1 == s2));
        System.out.println("s1 == s3: " + (s1 == s3));
        System.out.println("s1 == s4: " + (s1 == s4));
        System.out.println("s1 == s4.intern(): " + (s1 == s4.intern()));
        System.out.println("s1 == \"He\" + \"llo\": " + (s1 == "He" + "llo"));
        System.out.println("s1 == \"He\" + s5: " + (s1 == ("He" + s5)));
        System.out.println("s1 == \"He\" + s6: " + (s1 == ("He" + s6)));
    }
}
