package com.spduniversity.javacore.concurrency.examples.executors;

public class RunnableDemo {

    public static void main(String[] args) {
        Runnable task = () -> {
            String threadName = Thread.currentThread().getName();
            System.out.println("Hello " + threadName);
        };

        task.run();

        Thread thread = new Thread(task);
        Thread thread1 = new Thread(task);
        Thread thread2 = new Thread(task);
        Thread thread3 = new Thread(task);
        Thread thread4 = new Thread(task);

        thread.start();
        thread1.start();
        thread2.start();
        thread3.start();
        thread4.start();

        System.out.println("Done!");
    }
}
