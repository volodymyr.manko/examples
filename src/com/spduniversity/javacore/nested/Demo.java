package com.spduniversity.javacore.nested;

import java.util.ArrayList;
import java.util.List;

public class Demo {
    public static void main(String[] args) {

        List<Car> cars = new ArrayList<>();
        cars.add(new Car("Lexus", 100000d));
        cars.add(new Car("Opel", 20000d));
        cars.add(new Car("Tesla", 300000d));
        cars.add(new Car("AAAl", 10000d));

//        Collections.sort();
        System.out.println(cars);
    }
}
