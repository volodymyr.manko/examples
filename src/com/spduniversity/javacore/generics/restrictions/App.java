package com.spduniversity.javacore.generics.restrictions;

public class App {
}

// You can't create instance with type T
class Gen<T extends Number> {
    private T ob;

    private T vals[];    // Correct!

    Gen() {
//        ob = new T();   // Error!
    }

    Gen(T obj, T[] numbers) {
        ob = obj;

        // Error!
//        vals = new T[10];

        vals = numbers;
    }

    public static void main(String[] args) {
//        Gen<Integer> gens[] = new Gen<Integer>[10];   // Incorrect

        Gen<?> gens[] = new Gen<?>[10];    // Correct!
    }
}
