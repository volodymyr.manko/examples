package com.spduniversity.javacore.maps.utility;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ArraysDemo {
    public static void main(String[] args) {
        // List<T> asList(T...)
        String[] animals = new String[]{ "Bear", "Lion", "Panda", "Wolf" };
        List<String> animalsList = Arrays.asList(animals); // fixed-size list
        System.out.println("strings: " + animalsList);
//        animalsList.remove(0);            // UnsupportedOperationException
//        animalsList.add("Rhinoceros");    // UnsupportedOperationException
        animalsList.set(0, "Armadilo");
        System.out.println("Updated Array: " + Arrays.toString(animals));

        // Modifiable ArrayList from an Array
        animalsList = new ArrayList(Arrays.asList(animals));
        animalsList = Arrays.asList("Lion", "Wolf", "Woodpecker");
        System.out.println(animalsList);

        List<String> fixedAnimalsList = Arrays.asList(new String[3]);
        System.out.println("fixedAnimalsList size: " + fixedAnimalsList.size());

        // Sorting: void sort(Object[]) - Uses Merge-sort with natural ordering
        Arrays.sort(animals);
        System.out.println(Arrays.toString(animals));

        // Sorting: void sort(int[]) - Uses quick sort
        int[] digits = {10, 1, 15, 123, 14, 77};
        Arrays.sort(digits);
        System.out.println("Sorted digits: " + Arrays.toString(digits));

        // Binary Search: int binarySearch(int[], int);
        System.out.println("index returned by binary search: " + Arrays.binarySearch(digits, 123));

        int[] newArray = Arrays.copyOf(digits, 14);
        System.out.println("newArray: " + Arrays.toString(newArray));

        int[] newArray1 = new int[10];
        System.arraycopy(digits, 0, newArray1, 0, digits.length);
        System.out.println("newArray1: " + Arrays.toString(newArray1));

        Arrays.fill(newArray, -1);
        System.out.println("Fill with 13: " + Arrays.toString(newArray));

        System.out.println("Equals? " + Arrays.equals(digits, newArray));
    }
}
