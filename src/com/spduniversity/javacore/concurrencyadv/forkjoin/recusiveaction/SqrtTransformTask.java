package com.spduniversity.javacore.concurrencyadv.forkjoin.recusiveaction;

import java.util.concurrent.RecursiveAction;

class SqrtTransformTask extends RecursiveAction {
    private final static int seqThreshold = 1000;
    private double data[];
    private int start;
    private int end;

    SqrtTransformTask(double[] data, int start, int end) {
        this.data = data;
        this.start = start;
        this.end = end;
    }

    @Override
    protected void compute() {
        if ((end - start) < seqThreshold) {
            System.out.println("Processing " + start + " : " + end);
            for (int i = start; i < end; i++) {
                data[i] = Math.sqrt(data[i]);
            }
        }
        else {
            int middle = (start + end) / 2;
            invokeAll(new SqrtTransformTask(data, start, middle),
                      new SqrtTransformTask(data, middle, end));
        }
    }
}
