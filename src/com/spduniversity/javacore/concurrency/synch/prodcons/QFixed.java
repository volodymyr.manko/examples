package com.spduniversity.javacore.concurrency.synch.prodcons;

public class QFixed implements Q {
    int packages;
    boolean valueSet = false;

    @Override
    public synchronized int get() {
        if (!valueSet) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("Received: " + packages);
        valueSet = false;
        notify();

        return packages;
    }

    @Override
    public synchronized void put(int packages) {
        if (valueSet) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        this.packages = packages;
        valueSet = true;
        System.out.println("Sent: " + this.packages);
        notify();

    }
}
