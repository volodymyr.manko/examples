package com.spduniversity.javacore.enums.methods;

enum Apple {
    Jonathan(10), GoldenDel(9), RedDel(11), Winesap(18), Cortland;

    private int price;

    Apple(int price) {
        this.price = price;
    }

    Apple() { price = -1; }

    int getPrice() {
        return price;
    }
}