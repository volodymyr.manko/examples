package com.spduniversity.javacore.io.characterstream;

import java.io.*;

public class CopyLinesAutoclosable {
    public static void main(String[] args) throws IOException {

        try (BufferedReader inputStream = new BufferedReader(new FileReader("bytecopydemo.txt"));
             PrintWriter outputStream = new PrintWriter(new FileWriter("characteroutput.txt"))) {

            String line;
            while ((line = inputStream.readLine()) != null) {
                outputStream.println(line);
            }
        }
    }
}
