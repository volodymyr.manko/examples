package com.spduniversity.javacore.enums.menu;

import java.util.Arrays;

enum Course {
    Appetizer(Food.Appetizer.class),
    MainCourse(Food.MainCourse.class),
    Dessert(Food.Dessert.class),
    Coffee(Food.Coffee.class);

    private Food[] values;

    // private constructor!
    Course(Class<? extends Food> kind) {
        values = kind.getEnumConstants();
    }

    @Override
    public String toString() {
        return "Course{" +
                "values=" + Arrays.toString(values) +
                '}';
    }
}