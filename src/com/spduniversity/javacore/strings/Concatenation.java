package com.spduniversity.javacore.strings;

public class Concatenation {
    public static void main(String[] args) {
        String name = "Alex";
        String message = "Hello " + name + ", age: " + 100;
        System.out.println(message);
    }
}
