package com.spduniversity.javacore.abstractintefaces.logger.exception;

import java.io.IOException;

public class TryWithResource {
	public static void main(String[] args) throws Exception {
		assert false;
		System.out.println("===");
//		catchException(true);
//		System.out.println("=====");
//		catchException(false);
	}

	private static void catchException(boolean my) {
		try {
			SuppressedExceptionSample sample = getSample(my);
			sample.test();
		} catch (Exception e) {
			System.out.println("Main exception: " + e.getMessage());

			Throwable[] suppressed = e.getSuppressed();
			for (Throwable throwable : suppressed) {
				System.out.println("suppressed: " + throwable.getMessage());
			}

		}
	}

	private static SuppressedExceptionSample getSample(boolean my) {
		SuppressedExceptionSample sample;
		if (my) {
			sample = new MySuppressedExceptionSample();
		} else {
			sample = new JavaSuppressedExceptionSample();
		}
		return sample;
	}

	static interface SuppressedExceptionSample {
		void test() throws Exception;
	}

	static class MySuppressedExceptionSample implements SuppressedExceptionSample {
		public void test() throws Exception {
			A resourceA = new A();
			B resourceB = new B();
			Exception mainException = null;
			try {
				System.out.println("Ok");
				 throw new IOException("Problem with file: My");
			} catch (Exception ex) {
				mainException = ex;
			} finally {
				try {
					resourceB.close();
				} catch (Exception e2) {
					if (mainException != null) {
						mainException.addSuppressed(e2);
					} else {
						mainException = e2;
					}
				}

				try {
					resourceA.close();
				} catch (Exception e2) {
					if (mainException != null) {
						mainException.addSuppressed(e2);
					} else {
						mainException = e2;
					}
				}
				if (mainException != null) {
					throw mainException;
				}
			}
		}
	}

	static class JavaSuppressedExceptionSample implements SuppressedExceptionSample {
		private static void tryWithResources() throws IOException, Exception {
			try (A a = new A(); B b = new B();) {
				System.out.println("Java");
				throw new IOException("Problem with file");
			}
		}

		@Override
		public void test() throws Exception {
			tryWithResources();
		}
	}

	static class A implements AutoCloseable {

		@Override
		public void close() throws Exception {
			System.out.println("Try to close Class A");
			System.out.println();
			throw new RuntimeException("Exception in class A");
		}

	}

	static class B implements AutoCloseable {

		@Override
		public void close() throws Exception {
			System.out.println("Try to close Class B ");
			System.out.println();
			throw new RuntimeException("Exception in class B");
		}

	}

}
