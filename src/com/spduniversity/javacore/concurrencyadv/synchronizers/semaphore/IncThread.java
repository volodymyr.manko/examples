package com.spduniversity.javacore.concurrencyadv.synchronizers.semaphore;

import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

class IncThread implements Runnable {
    private String name;
    private Semaphore semaphore;

    IncThread(Semaphore semaphore, String name) {
        this.semaphore = semaphore;
        this.name = name;
        new Thread(this).start();
    }

    @Override
    public void run() {
        System.out.println("Start thread " + name);

        try {
            System.out.println("Thread " + name + " is waiting permission");
            semaphore.acquire();
            System.out.println("Thread " + name + " has get permission");

            for (int i = 0; i < 5; i++) {
                Shared.count++;
                System.out.println(name + " : " + Shared.count);
                Thread.sleep(100);
            }

        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("Release thread " + name);
        semaphore.release();
    }
}
