package com.spduniversity.javacore.concurrency.synch;

public class SynchDemo {
    public static void main(String[] args) {
        Callme target = new Callme();

        Caller job1 = new Caller("Welcome", target);
        Caller job2 = new Caller("to", target);
        Caller job3 = new Caller("the Java World!", target);

        try {
            job1.t.join();
            job2.t.join();
            job3.t.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
