package com.spduniversity.javacore.annotations.jobtitle;

public @interface Author {
    String value();
    JobTitle title() default JobTitle.UNKNOWN;
}
