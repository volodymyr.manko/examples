package com.spduniversity.javacore.enums.hardcore;

import java.util.EnumMap;
import java.util.function.BiConsumer;

public class EnumMaps {
    public static void main(String[] args) {
        EnumMap<AlarmPoints, Command> em = new EnumMap<>(AlarmPoints.class);

        em.put(AlarmPoints.Kitchen, new Command() {
            @Override
            public void action() {
                System.out.println("Kitchen in fire!");
            }
        });
        em.put(AlarmPoints.Bathroom, new Command() {
            @Override
            public void action() {
                System.out.println("Bathroom in fire!");
            }
        });
        boolean result = em.containsKey(AlarmPoints.Office3);
        System.out.println(result);

        em.forEach(new BiConsumer<AlarmPoints, Command>() {
            @Override
            public void accept(AlarmPoints k, Command v) {
                v.action();
            }
        });
    }
}
