package com.spduniversity.javacore.reflection.proxy.core;

public interface Interface {
    void doSomething();

    void doSomethingElse(String arg);
}
