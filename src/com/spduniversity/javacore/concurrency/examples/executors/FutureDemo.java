package com.spduniversity.javacore.concurrency.examples.executors;

import java.util.concurrent.*;

public class FutureDemo {

    public static void main(String[] args) {

        ExecutorService executor = Executors.newFixedThreadPool(3);

        Callable<Integer> task = () -> {
            TimeUnit.SECONDS.sleep(3);
            return 123;
        };

        // We can get result, but not immediately
        Future<Integer> future = executor.submit(task);

        System.out.println("future done? " + future.isDone());

        // Try to shut down threads
//        executor.shutdown();
//        executor.shutdownNow();

        Integer result = null;
        try {
            result = future.get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        System.out.println("future done? " + future.isDone());
        System.out.print("result: " + result);
    }
}
