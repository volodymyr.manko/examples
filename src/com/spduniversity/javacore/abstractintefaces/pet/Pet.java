package com.spduniversity.javacore.abstractintefaces.pet;

public abstract class Pet 
		implements Animal, LifeBeing {
	protected String name;
	protected int age;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


	public Pet(String name) {
		this.name = name;
	}

	public Pet() {
	}
	
	public abstract void voice();
	
	public void barking(){
		String saySomething = saySomething();
		System.out.println("I am running. "
				+ saySomething);
		
		move();
		
		System.out.println("I have stopped");
	}

	abstract String saySomething();
}
