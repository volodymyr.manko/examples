import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Arrays;

public class Sorter {
    public static void main(String[] args) throws IOException {
        final Reader in = new FileReader("input.txt");
        String[] lines = readAndSort(in);

        for (String s : lines) {
            System.out.println(s);
        }

        System.out.println("aaaabbbbcccc");

        System.out.println();
    }

    static String[] readAndSort(Reader in) throws IOException {
        BufferedReader reader = new BufferedReader(in);
        String line;
        final ArrayList<String> strings = new ArrayList<>();
        while ((line = reader.readLine())!=null) {
            strings.add(line);
        }
        reader.close();

        String[] lines = strings.toArray(new String[strings.size()]);
        Arrays.sort(lines);
        return lines;
    }
}
