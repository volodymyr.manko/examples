package com.spduniversity.javacore.abstractintefaces.pet.button;

import com.spduniversity.javacore.abstractintefaces.pet.Animal;
import com.spduniversity.javacore.abstractintefaces.pet.LifeBeing;

public class Main {
	public static void main(String[] args) {
		keyPressed(1);
		keyPressed(10);
		keyPressed(11);
		keyPressed(9);
		
		AnimalLifeBeing lifeBeing = new AnimalLifeBeing();
		printLifeBeing(lifeBeing);
		printAnimal(lifeBeing);
	}
	
	static class AnimalLifeBeing implements LifeBeing, Animal {

		@Override
		public void voice() {
		}

		@Override
		public void be() {
		}
		
	}
	
	static void printLifeBeing(LifeBeing lifeBeing){
		
	}
	
	static void printAnimal(Animal animal){
		
	}

	private static void keyPressed(int key) {
		if (key > 10){
			printText(key, new ICallback() {
				@Override
				public void doSomething() {
					System.out.println("Число більше 10");
				}
			});
		} else {
			printText(key, 
					() -> System.out.println("Key : " + key)
			);
		}
	}

	private static void printText(Integer key, ICallback callback) {
		System.out.println("Key : " + key);
		
		if (callback != null){
			callback.doSomething();
		}
	}
}
