package com.spduniversity.javacore.concurrencyadv.synchronizers.countdown;

import java.util.concurrent.CountDownLatch;

public class CountDownLatchDemo {
    public static void main(String[] args) {
        CountDownLatch cdl = new CountDownLatch(3);

        System.out.println("Thread is starting ...");

        new MyThread(cdl);
        new MyThread(cdl);
        new MyThread(cdl);

        try {
            cdl.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("Thread is stopping ...");
    }
}
