package com.spduniversity.javacore.staticfinal;

public class CarDemo {
    public static void main(String[] args) {
        Car opel = new Car("Opel");
        Car bmw = new Car("BMW");
        Car lexus = new Car("Lexus");

//        Car.accelerate();
//        Math.min(2, 5);
        opel = null;
        opel.accelerate();

//        System.out.println(Car.getCount());
    }
}
