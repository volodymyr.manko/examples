package com.spduniversity.javacore.concurrency.examples.executors;

import java.util.concurrent.TimeUnit;

public class RunnableSleepDemo {

    public static void main(String[] args) {

        // Task
        Runnable runnable = () -> {
            try {
                String name = Thread.currentThread().getName();
                System.out.println("Before " + name);
                TimeUnit.SECONDS.sleep(1);
                System.out.println("After " + name);
            }
            catch (InterruptedException e) {
                e.printStackTrace();
            }
        };

        Thread thread = new Thread(runnable);

        thread.start();
    }
}
