package com.spduniversity.javacore.enums.menu;

public interface Food {
    enum Appetizer implements Food {
        Salad, Soup, SprintingRolls;
    }

    enum MainCourse implements Food {
        Lasagne, Buritto, PadThai, Lentils;
    }

    enum Dessert implements Food {
        Fruit, Tiramisu, BlackForestCake;
    }

    enum Coffee implements Food {
        BlackCoffe, Espresso, Latte;
    }
}
