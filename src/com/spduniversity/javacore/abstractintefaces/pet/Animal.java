package com.spduniversity.javacore.abstractintefaces.pet;

public interface Animal extends LifeBeing {

	public static Integer MAX_PET_PAWS = 4;
	
	void voice();
	
	default void move(){
		System.out.println("Step + 1");
	}

	default void be(){
		
	}
	
}
