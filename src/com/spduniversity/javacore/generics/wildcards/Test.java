package com.spduniversity.javacore.generics.wildcards;

import java.util.ArrayList;
import java.util.List;

public class Test {
    static void printList(List<?> list) {
        for (Object obj : list) {
            System.out.println("{" + obj + "}");
        }
    }

    static Double sum(List<? extends Number> list) {
        Double result = 0.0;

        for (Number num : list) {
            result += num.doubleValue();
        }

        return result;
    }

    static <T extends Comparable<T>> T min(T a, T b) {
        if (a.compareTo(b) > 0) {
            return a;
        }
        return b;
    }



    public static void main(String[] args) {
        List<Integer> list1 = new ArrayList<Integer>();
        list1.add(new Integer(10));
        list1.add(20);
        printList(list1);
        System.out.println(sum(list1));

        List<String> list2 = new ArrayList<>();
        list2.add("one");
        list2.add("two");
        list2.add("three");
        printList(list2);
//        System.out.println(sum(list2));   // Error! String не является наследником Number

        List<Double> numList = new ArrayList<>();
        numList.add(1.0);
        numList.add(2.0);
        System.out.println(sum(numList));

        System.out.println(min(10, 20));
        System.out.println(min(10.0, 20.0));
        System.out.println(min("Hello", "Hello"));
        // Car and SuperCar should implement Comparable
        System.out.println(min(new Car(), new SuperCar()));
    }
}
