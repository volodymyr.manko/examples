package com.spduniversity.javacore.concurrencyadv.lock;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

class LockThread implements Runnable {
    private final ReentrantLock lock;
    private final String name;

    LockThread(ReentrantLock lock, String name) {
        this.lock = lock;
        this.name = name;
        new Thread(this).start();
    }

    @Override
    public void run() {
        System.out.println("Start thread: " + name);

        try {
            System.out.println("Thread " + name + " is waiting for blocking counter");
            lock.lock();
            System.out.println("Thread " + name + " has blocked counter");
            Shared.counter++;
            System.out.println("Thread " + name + " : " + Shared.counter);
            System.out.println("Thread " + name + " is waiting...");
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            System.out.println("Thread " + name + " release counter");
            lock.unlock();
        }
    }
}
