package com.spduniversity.javacore.abstractintefaces.pet;

public class Cat extends Pet {
	public Cat() {
	}
	
	public Cat(String name) {
//		super();
		this.name =name;
	}
	
	
	public void voice(){
		System.out.println("Мяв-мяв. Мене звати " + name);
	}
	
	@Override
	String saySomething() {
		return "I am a cat";
	}

	@Override
	public void be() {
	}

	
}
