package com.spduniversity.javacore.oop.testing;

public class Staff extends User {
    public void postReview() {
        System.out.println("Staff post a review.");
    }
}
