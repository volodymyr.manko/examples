package com.spduniversity.javacore.generics.wildcards.interfaces;

public class App {
    public static void main(String[] args) {
        Integer nums[] = {10, 2, 50, 1, 22};
        MyClass<Integer> iob = new MyClass<>(nums);
        System.out.println(iob.min());

        Character chars[] = {'b', 't', 'a'};
        MyClass<Character> cob = new MyClass<>(chars);
        System.out.println(cob.max());
    }
}
