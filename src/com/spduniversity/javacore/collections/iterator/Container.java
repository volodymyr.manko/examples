package com.spduniversity.javacore.collections.iterator;

public interface Container {
    Iterator getIterator();
}
