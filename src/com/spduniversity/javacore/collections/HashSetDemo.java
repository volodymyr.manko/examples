package com.spduniversity.javacore.collections;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.TreeSet;

public class HashSetDemo {
    public static void main(String[] args) {
        Set<String> cars = new LinkedHashSet<>();

        cars.add("Opel");
        cars.add("Mazda");
        cars.add("Lexus");
        cars.add("BMW");
        cars.add("Mersedes");
        cars.add("Lexus");
        cars.add(null);
        cars.add(null);
        cars.add(null);
        cars.add("ajsdklf");
        System.out.println(cars);

        Set<Car> cars1 = new LinkedHashSet<>();
        cars1.add(new Car("188i", 25000, "BMW", 2000));
        cars1.add(new Car("188i", 25000, "BMW", 2000));
        cars1.add(null);
        cars1.add(null);
        cars1.add(null);
        System.out.println(cars1);
    }
}
