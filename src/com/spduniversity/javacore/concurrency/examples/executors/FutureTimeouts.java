package com.spduniversity.javacore.concurrency.examples.executors;

import java.util.concurrent.*;

public class FutureTimeouts {

    public static void main(String[] args) {

        ExecutorService executor = Executors.newFixedThreadPool(2);

        Callable<Integer> task = () -> {
            try {
                TimeUnit.SECONDS.sleep(2);
            }
            catch (InterruptedException e) {
                System.out.println("task interrupted");
            }
            return 123;
        };

        Future<Integer> future = executor.submit(task);

        Integer result = null;
        try {
            result = future.get(1, TimeUnit.SECONDS);
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            e.printStackTrace();
        }

        System.out.println("result: " + result);

        executor.shutdownNow();
    }
}
