package com.spduniversity.javacore.enums.hardcore;

enum AlarmPoints {
    Stair1, Stair2, Kitchen, Office1, Office2, Office3, Office4, Bathroom
}
