package com.spduniversity.javacore.abstractintefaces.pet;

public class Friend {
	
	Animal friend;
	
	
	
	public Animal getFriend() {
		return friend;
	}

	public void setFriend(Animal friend) {
		this.friend = friend;
	}

//	@Override
	public void voice() {
		friend.voice();
	}

}
