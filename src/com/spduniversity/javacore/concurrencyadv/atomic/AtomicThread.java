package com.spduniversity.javacore.concurrencyadv.atomic;

import java.util.concurrent.TimeUnit;

class AtomicThread implements Runnable {
    private String name;

    AtomicThread(String name) {
        this.name = name;
        new Thread(this).start();
    }

    @Override
    public void run() {
        System.out.println("Start thread " + name);
        for (int i = 1; i <= 3; i++) {
            Shared.counter++;
            System.out.println("Thread " + name + " received: "
                    + Shared.ai.incrementAndGet() + " : "
                    + Shared.counter);
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
