package com.spduniversity.javacore.generics.simple;

public class BoxPrinter {
    private Object val;

    public BoxPrinter(Object arg) {
        this.val = arg;
    }

    public Object getVal() {
        return val;
    }

    @Override
    public String toString() {
        return "BoxPrinter{" +
                "val=" + val +
                '}';
    }

    public static void main(String[] args) {
        BoxPrinter printer = new BoxPrinter(10);
        System.out.println(printer);
        Integer result = (Integer) printer.getVal();

        BoxPrinter anotherPrinter = new BoxPrinter("Hello all!");
        System.out.println(anotherPrinter);
        // Problem is here!
        Integer anotherResult = (Integer) anotherPrinter.getVal();
    }
}
