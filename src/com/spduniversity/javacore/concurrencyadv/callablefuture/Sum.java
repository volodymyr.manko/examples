package com.spduniversity.javacore.concurrencyadv.callablefuture;

import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

class Sum implements Callable<Integer> {
    private int limit;

    Sum(int limit) {
        this.limit = limit;
    }

    @Override
    public Integer call() throws Exception {
        int sum = 0;

        for (int i = 1; i <= limit; i++) {
            sum += i;
        }
        TimeUnit.SECONDS.sleep(5);
        return sum;
    }
}
