package com.spduniversity.javacore.oop.dynamic;

class Dispatch {
    public static void main(String[] args) {
        A a = new A();
        B b = new B();
        C c = new C();

        A r;

        r = a;
        r.callme();     // Class A

        r = b;
        r.callme();     // Class B

        r = c;
        r.callme();     // Class C
    }
}
