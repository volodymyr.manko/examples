package com.spduniversity.javacore.staticfinal;

public class AwesomeUtils {
    private AwesomeUtils() {}

    public static int min(int a, int b) {
        return Math.min(a, b);
    }
}
