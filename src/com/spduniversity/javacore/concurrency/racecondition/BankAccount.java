package com.spduniversity.javacore.concurrency.racecondition;

class BankAccount implements Runnable {
    private int balance;

    @Override
    public void run() {
        withdraw(75);

        if (balance < 0) {
            System.out.println("Not enough money to withdraw!");
        }
    }

    private void withdraw(int amount) {
        if (balance >= amount) {
            System.out.println(Thread.currentThread().getName()
                    + " trying to withdraw " + amount);
             balance -= amount;

            System.out.println(Thread.currentThread().getName()
                    + " has withdrawn " + amount + " balance: " + balance);
        } else {
            System.out.println("Sorry, not enough money "
                    + Thread.currentThread().getName()
                    + " balance: " + balance);
        }
    }

    void setBalance(int balance) {
        this.balance = balance;
    }
}
