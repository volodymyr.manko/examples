package com.spduniversity.javacore.concurrency;

import java.util.concurrent.TimeUnit;

public class ThreadDemo {

    public static void main(String[] args) {
        Task task = new Task();
        Thread thread = new Thread(task);   // New
        thread.start();     // Dead after execution

//        thread.start();     // IllegalThreadStateException

        try {
            TimeUnit.SECONDS.sleep(3);  // Running -> Blocked -> Runnable (after 3 sec)
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("Inside main() method");
    }
}