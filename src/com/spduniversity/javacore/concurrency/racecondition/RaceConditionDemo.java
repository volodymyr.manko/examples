package com.spduniversity.javacore.concurrency.racecondition;

public class RaceConditionDemo {

    public static void main(String[] args) {
        BankAccount account = new BankAccount();
        account.setBalance(100);

        Thread bill = new Thread(account);
        Thread john = new Thread(account);

        bill.setName("Bill");
        john.setName("John");

        john.start();
        bill.start();
    }
}
