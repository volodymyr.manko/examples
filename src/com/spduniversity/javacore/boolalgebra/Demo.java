package com.spduniversity.javacore.boolalgebra;

public class Demo {
    public static void main(String[] args) {
        checkGlue(true, false);
    }

    private static boolean checkGlue(boolean a, boolean b) {
        return (a && b || !a && b);
    }

    private static boolean checkDeMorgan(boolean a, boolean b) {
//        return !a && !b;
        return !a || !b;
    }

    private static boolean checkComplement(boolean a) {
        return (a && !a);
//        return a || !a;
    }

    private static boolean checkBlake(boolean a, boolean b) {
        return a || b;
//        return a && (!a || b);
    }

    // Не умеет упрощать :)
    private static boolean checkDoubleNegation(boolean a) {
        return !!a;
    }

    // Не умеет упрощать :)
    private static boolean checkDistribLaw(boolean a, boolean b, boolean c) {
        return (a || b) && (a || c);
    }

}
