package com.spduniversity.javacore.generics.simple;

public class BoxPrinterGen<T> {
    private T val;

    BoxPrinterGen(T val) {
        this.val = val;
    }

    @Override
    public String toString() {
        return "BoxPrinterGen{" +
                "val=" + val +
                '}';
    }

    T getVal() {
        return val;
    }

    public static void main(String[] args) {
        BoxPrinterGen<Integer> printer = new BoxPrinterGen<Integer>(10);
        System.out.println(printer);
        Integer result = (Integer) printer.getVal();

        BoxPrinterGen<String> anotherPrinter = new BoxPrinterGen<String>("Hello all!");
        System.out.println(anotherPrinter);
        // Problem is here!
        String anotherResult = anotherPrinter.getVal();
    }
}
