package com.spduniversity.javacore.abstractintefaces.logger.exception;

public class MyCheckedException extends Exception {

	public MyCheckedException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public MyCheckedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public MyCheckedException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public MyCheckedException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public MyCheckedException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
