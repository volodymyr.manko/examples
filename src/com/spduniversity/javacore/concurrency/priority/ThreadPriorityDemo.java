package com.spduniversity.javacore.concurrency.priority;

public class ThreadPriorityDemo {

    public static void main(String[] args) {
        // Thread[main, 5, main] (name, priority, thread group)
        System.out.println(Thread.currentThread());

        Thread imagesThread = new Thread(new DownloadImagesTask());
        Thread priceAggregatorThread = new Thread(new PriceAggregator());
        Thread dataCrawlerThread = new Thread(new DataCrawler());

        imagesThread.setName("downloadImages");
        priceAggregatorThread.setName("priceAggregator");
        dataCrawlerThread.setName("dataCrawler");

        imagesThread.setPriority(Thread.MAX_PRIORITY);
        dataCrawlerThread.setPriority(Thread.NORM_PRIORITY);
        priceAggregatorThread.setPriority(Thread.MIN_PRIORITY);

        imagesThread.start();
        dataCrawlerThread.start();
        priceAggregatorThread.start();

        try {
            imagesThread.join(1);   // Waiting for end of the imagesThread
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("End main()...");
    }
}
