package com.spduniversity.javacore.enums.methods;

public class AppleDemo3 {
    public static void main(String[] args) {
//        Apple apple;

        System.out.println("Яблоки сорта Jonathan стоят: " + Apple.Jonathan.getPrice() + " центов.\n");

        System.out.println("Цены на все сорта яблок:");
        for (Apple app : Apple.values()) {
            System.out.println("Яблоки сорта " + app + " стоят " + app.getPrice() + " центов.");
        }
    }
}
