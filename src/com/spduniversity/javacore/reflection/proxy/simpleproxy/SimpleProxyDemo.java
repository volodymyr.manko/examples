package com.spduniversity.javacore.reflection.proxy.simpleproxy;

import com.spduniversity.javacore.reflection.proxy.core.Interface;
import com.spduniversity.javacore.reflection.proxy.core.RealObject;

class SimpleProxyDemo {

    private static void consumer(Interface iface) {
        iface.doSomething();
        iface.doSomethingElse("My args");
    }

    public static void main(String[] args) {
        consumer(new RealObject());

        // "Оборачиваем" объект в proxy
        consumer(new SimpleProxy(new RealObject()));
    }
}