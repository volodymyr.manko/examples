package com.spduniversity.javacore.annotations.simple;

public class TypedAnnoDemo {

    public int someMethod(@TypedAnno TypedAnnoDemo this, int i, int j) {
        return i + j;
    }

    public @TypedAnno Integer f(int i, int j) {
        return i + j;
    }

    public static void main(String[] args) {
        TypedAnnoDemo demo = new TypedAnnoDemo();
        int result = demo.someMethod(10, 20);
        System.out.println(result);
    }
}
