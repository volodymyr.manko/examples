package com.spduniversity.javacore.io.file;

import java.io.File;
import java.nio.file.Paths;

public class DirList {
    public static void main(String[] args) {
        String dirName = Paths.get(".").toAbsolutePath().normalize() + "/";
        File dir = new File(dirName);

        if (dir.isDirectory()) {
            System.out.println("Directory: " + dir.getName());
            String items[] = dir.list();

            for (String item : items) {
                File file = new File(dirName + item);
                if (file.isDirectory()) {
                    System.out.println(item + " is a directory");
                }
                else {
                    System.out.println(item + " is a file");
                }
            }
        }
    }
}
