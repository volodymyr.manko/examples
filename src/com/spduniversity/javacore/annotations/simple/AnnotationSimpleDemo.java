package com.spduniversity.javacore.annotations.simple;

public class AnnotationSimpleDemo {

    @MyAnnotation(text = "My annotation")
    public void someMethod() {}

    @MySingle(10)
    public void anotherMethod() {}
}
