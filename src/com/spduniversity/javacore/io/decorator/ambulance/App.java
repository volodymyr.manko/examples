package com.spduniversity.javacore.io.decorator.ambulance;

public class App {
    public static void main(String[] args) {
        DecoratorCar doctorsDream = new FlyingCar(new Mersedes());
        doctorsDream.go();
        doctorsDream.test();
    }
}