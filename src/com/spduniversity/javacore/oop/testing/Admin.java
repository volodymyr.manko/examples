package com.spduniversity.javacore.oop.testing;

public class Admin extends Staff {
    public void addUser(String login) {
        System.out.println("Admin add user with login " + login);
    }

    public void removeUser(String login) {
        System.out.println("Admin remove user with login " + login);
    }
}
