package com.spduniversity.javacore.io.file;

import java.io.File;
import java.io.IOException;

public class FileDemo {
    public static void main(String[] args) throws IOException {

        File file = new File("bytecopydemo.txt");

        System.out.println("File name: " + file.getName());
        System.out.println("Path: " + file.getPath());
        System.out.println("Absolute path: " + file.getAbsolutePath());
        System.out.println("Parent: " + file.getParent());

        System.out.println("Is exists? " + file.exists());
        System.out.println("Can read? " + file.canRead());
        System.out.println("Can write? " + file.canWrite());
        System.out.println("Is directory? " + file.isDirectory());
        System.out.println("Is file? " + file.isFile());

        System.out.println("Last modified: " + file.lastModified());
        System.out.println("Size: " + file.length() + " bytes.");
    }

}