package com.spduniversity.javacore.oop.dynamic;

class B extends A {
    void callme() {
        System.out.println("B.callme()");
    }
}
