package com.spduniversity.javacore.oop.testing;

public class Moderator extends Staff {
    public void approveReview() {
        System.out.println("Moderator have approved a review.");
    }

    public void rejectReview() {
        System.out.println("Moderator have rejected a review.");
    }

}
