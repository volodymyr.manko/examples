package com.spduniversity.javacore.oop.modifiers.pack1;

public class MyClassA {
    static private int privateField = 1;
    static int defaultField = 2;
    static protected int protectedField = 3;
    static public int publicField = 4;

    public static void main(String[] args) {
        System.out.println(privateField);
        System.out.println(defaultField);
        System.out.println(protectedField);
        System.out.println(publicField);
    }
}
