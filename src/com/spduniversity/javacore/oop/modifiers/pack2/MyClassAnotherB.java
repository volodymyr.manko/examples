package com.spduniversity.javacore.oop.modifiers.pack2;

import com.spduniversity.javacore.oop.modifiers.pack1.MyClassA;
import com.spduniversity.javacore.oop.modifiers.pack1.MyClassC;

public class MyClassAnotherB {
    public static void main(String[] args) {
        System.out.println(MyClassA.publicField);
        System.out.println(MyClassC.publicField);
        System.out.println(MyClassAnotherC.publicField);
    }
}
