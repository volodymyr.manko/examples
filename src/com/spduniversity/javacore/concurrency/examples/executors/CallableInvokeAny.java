package com.spduniversity.javacore.concurrency.examples.executors;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.*;

public class CallableInvokeAny {

    public static void main(String[] args) throws ExecutionException, InterruptedException {

        ExecutorService executor = Executors.newWorkStealingPool();

        List<Callable<String>> tasks = Arrays.asList(
          callable("task1", 3),
          callable("task2", 1),
          callable("task3", 2)
        );

        String result = executor.invokeAny(tasks);
        System.out.println(result);
    }

    // Helper
    private static Callable<String> callable(String result, long sleepSeconds) {
        return () -> {
            TimeUnit.SECONDS.sleep(sleepSeconds);
            return result;
        };
    }
}
