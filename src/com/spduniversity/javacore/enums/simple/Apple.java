package com.spduniversity.javacore.enums.simple;

enum Apple {
    Jonathan, GoldenDel, RedDel, Winesap, Cortland
}
