package com.spduniversity.javacore.oop.testing;

public class ChiefModerator extends Moderator {

    public void allowAllApprovedReviews() {
        System.out.println("Chief Moderator have allowed all approved reviews.");
    }

}
