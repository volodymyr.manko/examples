package com.spduniversity.javacore.annotations.simple;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

// public @interface MyAnnotation extends Annotation {  // Error!

@Retention(RetentionPolicy.RUNTIME)
public @interface MyAnnotation {
    String text();
    int val() default 1000;
}
