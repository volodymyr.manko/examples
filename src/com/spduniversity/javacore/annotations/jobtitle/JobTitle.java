package com.spduniversity.javacore.annotations.jobtitle;

public enum JobTitle {
    JUNIOR, MIDDLE, SENIOR, LEAD, UNKNOWN
}
