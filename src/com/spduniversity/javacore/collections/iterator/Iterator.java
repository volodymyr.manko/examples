package com.spduniversity.javacore.collections.iterator;

public interface Iterator {
    boolean hasNext();
    Object next();
}
