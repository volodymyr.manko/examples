package com.spduniversity.javacore.maps;

import java.util.*;

public class HashMapDemo {
    public static void main(String[] args) {
//        simpleMap();
//        complexMap();
//        mutableKeysDemo();
        immutableKeysDemo();
    }

    private static void immutableKeysDemo() {
        Car car = new Car("Lexus RX300", 50000);
        Map<Car, String> map = new HashMap<>();
        map.put(car, "Lexus");
        map.put(car, "Lexus2");
        map.put(car, "Lexus4");
        System.out.println(map);

        car.setName("BMW");
        System.out.println(map.get(car));
    }

    private static void mutableKeysDemo() {
        List<Integer> list = new ArrayList<>();
        list.add(1);

        Map<List<Integer>, Integer> map = new HashMap<>();
        map.put(list, 1);
        System.out.println(map.get(list));  // return 1

        list.add(2);
        list.remove(1);
        System.out.println(map.get(list)); // return null
    }

    private static void complexMap() {
        Map<String, Map<String, Object>> carProfile = new HashMap<>();

        Map<String, Object> profile = new HashMap<>();
        profile.put("Brand", "BMW");
        profile.put("Year", 2016);
        profile.put("Speed", 190);
        carProfile.put("118i", profile);

        profile = new HashMap<>();
        profile.put("Brand", "Lexus");
        profile.put("Year", 2015);
        profile.put("Speed", 200);
        carProfile.put("RX300", profile);

        profile = new HashMap<>();
        profile.put("Brand", "Opel");
        profile.put("Year", 2014);
        profile.put("Speed", 185);
        carProfile.put("Astra", profile);

        System.out.println(carProfile);

        final Map<String, Object> rx300Profile = carProfile.get("RX300");
        String brand = (String) rx300Profile.get("Brand");
        int year = (Integer) rx300Profile.get("Year");
        int speed = (Integer) rx300Profile.get("Speed");
        System.out.println("rx300 Profile: {" + "brand: " + brand + ", year: " + year + ", speed: " + speed + "}");
    }

    private static void simpleMap() {
        Map<String, Integer> cars = new HashMap<>();
        cars.put("118i", 50000);
        cars.put("Astra", 20000);
        cars.put("LX300", 100000);
        cars.put(null, 0);
        cars.put(null, 10);
        cars.put("Kangoo", null);
        System.out.println(cars);

        cars.put("Kangoo", 12000);
        System.out.println(cars);

        // Contains operations
        System.out.println("Contains key=LX300? " + cars.containsKey("LX300"));
        System.out.println("Contains value = 20000? " + cars.containsValue(20000));
        System.out.println("LX300 price: " + cars.get("LX300"));
        System.out.println();

        System.out.print("Remove operation: ");
        cars.remove("Kangoo");
        System.out.println(cars);
        System.out.println();

        // Итерация с помощью keySet()
        Set<String> modelNames = cars.keySet();
        for (String modelName : modelNames) {
            System.out.println("Model: " + modelName + ", price: " + cars.get(modelName));
        }
        System.out.println();

        // Итерация с помощью entrySet()
        final Set<Map.Entry<String, Integer>> entries = cars.entrySet();
        for (Map.Entry<String, Integer> entry : entries) {
            System.out.println("Model: " + entry.getKey() + ", price: " + entry.getValue());
        }
        System.out.println();
    }
}

class Car {
    private String name;
    private int price;

    public Car(String name, int price) {
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

//    @Override
//    public int hashCode() {
//        return new Random().nextInt();
//    }


    @Override
    public boolean equals(Object obj) {
        return false;
    }
}