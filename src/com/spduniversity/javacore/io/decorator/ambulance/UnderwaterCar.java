package com.spduniversity.javacore.io.decorator.ambulance;

class UnderwaterCar extends DecoratorCar {
    UnderwaterCar(Car decoratedCar) {
        super(decoratedCar);
    }

    @Override
    public void go() {
        super.go();
        System.out.println("... Dive!");
    }

    @Override
    public void test() {
        super.test();
        System.out.println("Hello all");
    }

}
