package com.spduniversity.javacore.enums.impl;

enum Apple implements Printable, Loggable {
    Golden, Jonathan, Semirenko;

    @Override
    public void print() {
        System.out.println("Print something from enum.");
    }

    public void log() {
        System.out.println("Logging something from enum.");
    }
}
