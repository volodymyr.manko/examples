package com.spduniversity.javacore.maps;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;

public class LinkedHashMapDemo {
    public static void main(String[] args) {
        Map<String, String> map = new TreeMap<String, String>();
//        Map<String, String> map = new LinkedHashMap<String, String>(16, 0.75f, true); // Works as LRU Cache
//        Map<String, String> map = new LRUCache<>(16, 0.75f, true);    // Works as LRU Cache (fixed)

        map.put("z", "ZED");
        map.put("a", "A");
        map.put("c", "C");
        map.put("b", "B");
        System.out.println(map);

        map.get("a");
        map.get("a");
        map.get("a");
        System.out.println(map);
        map.get("b");
        System.out.println(map);

        map.put("e", "E");
        System.out.println(map);
        map.put("f", "F");
        System.out.println(map);
    }

}

class LRUCache<U,V> extends LinkedHashMap<U,V> {
    private static final int MAX_ENTRIES = 4;

    public LRUCache(int initialCapacity, float loadFactor, boolean accessOrder) {
        super(initialCapacity, loadFactor, accessOrder);
    }

    @Override
    protected boolean removeEldestEntry(Map.Entry<U, V> eldest) {
        return size() > MAX_ENTRIES;
    }
}