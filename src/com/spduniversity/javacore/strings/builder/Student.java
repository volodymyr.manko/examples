package com.spduniversity.javacore.strings.builder;

import java.util.List;

class Student {
    private String name;
    private int age;
    private List<String> language;

    static class Builder {

        private String name;
        private int age;
        private List<String> language;

        Builder name(String name) {
            this.name = name;
            return this;
        }

        Builder age(int age) {
            this.age = age;
            return this;
        }

        Builder language(List<String> language) {
            this.language = language;
            return this;
        }

        Student build() {
            return new Student(this);
        }

    }

    private Student(Builder builder) {
        name = builder.name;
        age = builder.age;
        language = builder.language;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", language=" + language +
                '}';
    }
}
