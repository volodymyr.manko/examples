package com.spduniversity.javacore.concurrency.synch;

class Caller implements Runnable {
    String msg;
    Callme target;
    Thread t;

    Caller(String msg, Callme target) {
        this.msg = msg;
        this.target = target;

        t = new Thread(this);
        t.start();
    }

//    @Override
//    public void run() {
//        target.call(msg);
//    }

    @Override
    public void run() {
        synchronized (target) {
            target.call(msg);
        }
    }
}
