package com.spduniversity.javacore.abstractintefaces.pet;

public class Dog extends Pet {
	public Dog() {
	}
	public Dog(String name) {
		super(name);
	}
	
	@Override
	public void voice() {
		System.out.println("Гав-гав. Мене звати " + name);
		System.out.println("I am " + age);
	}

	@Override
	String saySomething() {
		return "I am a dog";
	}

}