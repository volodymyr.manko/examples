package com.spduniversity.javacore.io.decorator.ambulance;

class AmbulanceCar extends DecoratorCar {

    AmbulanceCar(Car decoratedCar) {
        super(decoratedCar);
    }

    @Override
    public void go() {
        super.go();
        System.out.println("... beep, beep, beep!");
    }
}
