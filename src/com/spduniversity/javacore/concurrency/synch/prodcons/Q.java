package com.spduniversity.javacore.concurrency.synch.prodcons;

public interface Q {
    int get();

    void put(int packages);
}
