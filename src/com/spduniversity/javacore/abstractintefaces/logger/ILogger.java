package com.spduniversity.javacore.abstractintefaces.logger;

public interface ILogger {
	void log(String msg);
	
	void error(Throwable e);
}
