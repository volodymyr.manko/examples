package com.spduniversity.javacore.concurrency.examples.synchronization;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.IntStream;

import static com.spduniversity.javacore.concurrency.examples.ConcurrentUtils.stop;

public class RaceConditionDemo {
    private static final int NUM_INCREMENTS = 10000;

    private static int count = 0;

    private static void increment() {
        count = count + 1;
    }

    private static synchronized void synchMethodIncrement() {
        count = count + 1;
    }

    private static void synchBlockIncrement() {
        synchronized (RaceConditionDemo.class) {
            count = count + 1;
        }
    }

    public static void main(String[] args) {

        ExecutorService executor = Executors.newFixedThreadPool(2);

//        IntStream.range(0, 10000)
//                .forEach(i -> executor.submit(RaceConditionDemo::increment));

        
//        IntStream.range(0, 10000)
//                .forEach(i -> executor.submit(RaceConditionDemo::synchMethodIncrement));

        IntStream.range(0, NUM_INCREMENTS)
                .forEach(i -> executor.submit(RaceConditionDemo::synchBlockIncrement));

        stop(executor);

        System.out.println("Count: " + RaceConditionDemo.count);
    }
}
