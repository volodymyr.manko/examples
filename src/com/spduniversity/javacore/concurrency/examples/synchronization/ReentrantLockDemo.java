package com.spduniversity.javacore.concurrency.examples.synchronization;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.IntStream;

import static com.spduniversity.javacore.concurrency.examples.ConcurrentUtils.stop;

public class ReentrantLockDemo {
    private static final int NUM_INCREMENTS = 10000;
    private static int count = 0;

    private static ReentrantLock lock = new ReentrantLock();

    private static void increment() {
        lock.lock();
        try {
            count = count + 1;
        }
        finally {
            lock.unlock();
        }
    }

    public static void main(String[] args) {

        ExecutorService executor = Executors.newFixedThreadPool(2);

        IntStream.range(0, NUM_INCREMENTS)
                .forEach(i -> executor.submit(ReentrantLockDemo::increment));

        stop(executor);
        System.out.println("Count : " + count);
    }
}
