package com.spduniversity.javacore.collections;


import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;

public class TreeSetDemo {
    public static void main(String[] args) {
        Car bmw118i = new Car("118i", 25000, "BMW", 2015);
        Car bmw118i1 = new Car("118i", 25000, "BMW", 2010);
        Car opelAstra = new Car("Astra", 20000, "Opel", 1990);
        Car lexusLx300 = new Car("Lx300", 100000, "Lexus", 2016);

        Set<Car> cars = new TreeSet<>();
        cars.add(bmw118i);
        cars.add(bmw118i1);
        cars.add(opelAstra);
        cars.add(lexusLx300);

        for (Car car : cars) {
            System.out.println(car);
        }
    }
}
