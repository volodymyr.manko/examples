package com.spduniversity.javacore.enums.impl;

public interface Loggable {
    void log();
}
