package com.spduniversity.javacore.concurrencyadv.atomic;

public class AtomicDemo {
    public static void main(String[] args) {
        for (int i = 0; i < 5; i++) {
            new AtomicThread("A"+i);
        }
    }
}
