package com.spduniversity.javacore.staticfinal;

public class Car {
    private static int count;
    private String model;

    public static void accelerate() {
        System.out.println("Accelerate");
    }

    public Car(String model) {
        this.model = model;

        count++;
    }

    public static int getCount() {
        return count;
    }
}
