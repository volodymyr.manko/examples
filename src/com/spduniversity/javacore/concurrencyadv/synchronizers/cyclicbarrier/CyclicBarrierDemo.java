package com.spduniversity.javacore.concurrencyadv.synchronizers.cyclicbarrier;

import java.util.concurrent.CyclicBarrier;

public class CyclicBarrierDemo {
    public static void main(String[] args) {
        CyclicBarrier barrier =
                new CyclicBarrier(5, () -> System.out.println("Barrier has reached!"));

        System.out.println("Start threads...");

        new MyThread(barrier, "A");
        new MyThread(barrier, "B");
        new MyThread(barrier, "C");
        new MyThread(barrier, "X");
        new MyThread(barrier, "Y");
        new MyThread(barrier, "Z");
    }
}
