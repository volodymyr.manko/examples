package com.spduniversity.javacore.annotations.jobtitle;

public @interface Version {
    int version();
    Date date();
    Author[] authors() default {};
    Class<?> previous() default Void.class;
}
