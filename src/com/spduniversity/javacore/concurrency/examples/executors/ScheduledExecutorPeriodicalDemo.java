package com.spduniversity.javacore.concurrency.examples.executors;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class ScheduledExecutorPeriodicalDemo {

    public static void main(String[] args) {

//        scheduleWithRate();
        scheduleWithFixedDelay();
    }

    private static void scheduleWithRate() {
        ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);

        Runnable task = () -> System.out.println("Scheduling: " + System.nanoTime());

        int initialDelay = 0;
        int period = 1;

        // Run task every 1 second infinitely
        // It doesn't take into account the actual duration of the task
        executor.scheduleAtFixedRate(task, initialDelay, period, TimeUnit.SECONDS);
    }

    private static void scheduleWithFixedDelay() {
        ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);

        Runnable task = () -> {
            try {
                TimeUnit.SECONDS.sleep(2);
                System.out.println("Scheduling: " + System.nanoTime());
            } catch (InterruptedException e) {
                System.err.println("task interrupted");
            }

        };

        long initialDelay = 0;
        long period = 1;

        // 2 second duration of task + 1 second delay
        executor.scheduleWithFixedDelay(task, initialDelay, period, TimeUnit.SECONDS);
    }
}