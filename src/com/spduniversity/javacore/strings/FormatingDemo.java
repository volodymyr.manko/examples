package com.spduniversity.javacore.strings;

import java.util.Formatter;

public class FormatingDemo {
    public static void main(String[] args) {
        int x = 10;
        double y = 20;
        // The first way
        System.out.format("Result: [%5d, %10.2f]\n", x, y);

        // The second way
        Formatter f = new Formatter(System.out);
        f.format("Result: [%5d, %10.2f]\n", x, y);

        // The Third way
        System.out.println(String.format("Result: [%5d, %10.2f]\n", x, y));
    }
}

