package com.spduniversity.javacore.reflection.proxy.dynproxy;

import com.spduniversity.javacore.reflection.proxy.core.Interface;
import com.spduniversity.javacore.reflection.proxy.core.RealObject;

import java.lang.reflect.Proxy;

public class SimpleDynamicProxy {

    static void consumer(Interface iface) {
        iface.doSomething();
        iface.doSomethingElse("Hello world!");
    }

    public static void main(String[] args) {
        final RealObject realObject = new RealObject();
        consumer(realObject);

        Interface proxy = (Interface) Proxy.newProxyInstance(
                Interface.class.getClassLoader(),   // needs ClassLoader
                new Class[]{Interface.class},       // list interfaces or abstract classes that you wish proxy to implement
                new DynamicProxyHandler(realObject)); // implementation of the interface InvocationHandler
        consumer(proxy);
    }
}
