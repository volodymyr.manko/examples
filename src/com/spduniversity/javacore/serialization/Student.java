package com.spduniversity.javacore.serialization;

import java.io.Serializable;

public class Student implements Serializable {
    private static final long serialVersionUID = 2297285140775528194L;
    private String name;
    private int age;

    private transient int id = 1;

    public Student(String name, int age) {
        this.name = name;
        this.age = age;
    }
    private String hobby;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", id=" + id +
                ", hobby='" + hobby + '\'' +
                '}';
    }
}
