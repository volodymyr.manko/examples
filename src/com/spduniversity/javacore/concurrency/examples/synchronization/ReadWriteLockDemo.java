package com.spduniversity.javacore.concurrency.examples.synchronization;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import static com.spduniversity.javacore.concurrency.examples.ConcurrentUtils.print;
import static com.spduniversity.javacore.concurrency.examples.ConcurrentUtils.stop;

public class ReadWriteLockDemo {

    public static void main(String[] args) {
        ExecutorService executor = Executors.newFixedThreadPool(2);
        ReadWriteLock lock = new ReentrantReadWriteLock();
        Map<String, String> map = new HashMap<>();

        Runnable writeTask = () -> {
            print("WriteLock (locked)");

            lock.writeLock().lock();
            try {
                TimeUnit.SECONDS.sleep(1);
                map.put("key", "value");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            finally {
                lock.writeLock().unlock();
                print("WriteLock (unlocked)");
            }
        };

        Runnable readTask = () -> {
            print("ReadLock (locked)");

            lock.readLock().lock();
            try {
                print("Map value: " + map.get("key"));
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                lock.readLock().unlock();
                print("ReadLock (unlocked)");
            }
        };

        // 1 writer
        executor.submit(writeTask);

        // 3 readers
        executor.submit(readTask);
        executor.submit(readTask);
        executor.submit(readTask);

        stop(executor);
    }
}
