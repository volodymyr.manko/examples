package com.spduniversity.javacore.generics.restrictions;

/**
 * Created by olegnovitskiy on 11/2/16.
 */
public class GenArrays {
    public static void main(String[] args) {
        Integer numbers[] = {1, 2, 3, 4, 5};

        Gen<Integer> intObject = new Gen<Integer>(10, numbers);
    }
}
