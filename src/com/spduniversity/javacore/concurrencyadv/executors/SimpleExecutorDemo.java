package com.spduniversity.javacore.concurrencyadv.executors;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class SimpleExecutorDemo {
    public static void main(String[] args) {
        CountDownLatch latch1 = new CountDownLatch(3);
        CountDownLatch latch2 = new CountDownLatch(3);
        CountDownLatch latch3 = new CountDownLatch(3);
        CountDownLatch latch4 = new CountDownLatch(3);

        // Pool with 2 threads
        ExecutorService exec = Executors.newFixedThreadPool(2);

        System.out.println("Threads are running");

        exec.execute(new MyThread(latch1, "A"));
        exec.execute(new MyThread(latch2, "B"));
        exec.execute(new MyThread(latch3, "C"));
        exec.execute(new MyThread(latch4, "D"));

        try {
            latch1.await();
            latch2.await();
            latch3.await();
            latch4.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // Stop all threads
        exec.shutdown();
    }
}
