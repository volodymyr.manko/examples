package com.spduniversity.javacore.enums.hardcore;

public interface Command {
    void action();
}
