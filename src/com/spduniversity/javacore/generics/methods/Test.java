package com.spduniversity.javacore.generics.methods;

import java.util.ArrayList;
import java.util.List;

class Utilities {
    // Generic method example
    static <T> void fill(List<T> list, T val) {
        for (int i = 0; i < list.size(); i++) {
            list.set(i, val);
        }
    }
}

public class Test {
    public static void main(String[] args) {
        List<Integer> list1 = new ArrayList<>();
        list1.add(100);
        list1.add(200);
        list1.add(300);
        System.out.println(list1);
        Utilities.fill(list1, 0);
        System.out.println(list1);

        List<String> list2 = new ArrayList<String>();
        list2.add("Hello");
        list2.add("World!");
        System.out.println(list2);
        Utilities.fill(list2, "Yo-ho-ho");
        System.out.println(list2);
    }
}
