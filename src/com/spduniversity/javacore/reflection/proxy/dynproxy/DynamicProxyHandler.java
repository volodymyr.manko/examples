package com.spduniversity.javacore.reflection.proxy.dynproxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

class DynamicProxyHandler implements InvocationHandler {
    private Object proxied;

    DynamicProxyHandler(Object proxied) {
        this.proxied = proxied;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println("\nProxy Before: " + proxy.getClass() + ", method: " + method);
        if (args != null) {
            System.out.println("Args: ");
            for (Object arg: args) {
                System.out.println(arg + " ");
            }
        }

        // Call method of the realObject
        Object result = method.invoke(proxied, args);

        System.out.println("Proxy After method");

        return result;
    }
}
