package com.spduniversity.javacore.concurrency.examples.synchronization;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

import static com.spduniversity.javacore.concurrency.examples.ConcurrentUtils.stop;

public class ReentrantLockTraceDemo {

    public static void main(String[] args) {

        ExecutorService executor = Executors.newFixedThreadPool(2);
        ReentrantLock lock = new ReentrantLock();

        executor.submit(()-> {
           lock.lock();
           try {
               TimeUnit.SECONDS.sleep(1);
           } catch (InterruptedException e) {
               e.printStackTrace();
           } finally {
               lock.unlock();
           }
        });

        executor.submit(() -> {
            System.out.println("Locked: " + lock.isLocked());
            System.out.println("Held by me: " + lock.isHeldByCurrentThread());

            boolean locked = lock.tryLock();

            System.out.println("Lock acquired: " + locked);
        });

        stop(executor);
    }
}
