package com.spduniversity.javacore.oop.overriding;

class A {
    public void method1(int a) {}
    public void method1(int a, int b) {}

    public void method1(char c) {
    }
}

class B extends A {
//    @Override
    public void method1(String name) {}
}

public class Demo {
    public static void main(String[] args) {
        B b = new B();
    }
}
