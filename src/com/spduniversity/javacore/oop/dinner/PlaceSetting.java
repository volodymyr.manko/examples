package com.spduniversity.javacore.oop.dinner;

class Plate {
    Plate(int i) {
        System.out.println("Plate constructor.");
    }
}

class DinnerPlate extends Plate {
    DinnerPlate(int i) {
        super(i);       // Try comment that constructor :)
        System.out.println("DinnerPlate constructor.");
    }
}

class Utensil {
    Utensil(int i) {
        System.out.println("Utensil constructor.");
    }
}

class Fork extends Utensil {
    Fork(int i) {
        super(i);
        System.out.println("Fork constructor.");
    }
}

class Spoon extends Utensil {
    public Spoon(int i) {
        super(i);
        System.out.println("Spoon constructor.");
    }
}

class Knife extends Utensil {
    Knife(int i) {
        super(i);
        System.out.println("Knife constructor.");
    }
}

class Custom {
    Custom(int i) {
        System.out.println("Custom constructor.");
    }
}

public class PlaceSetting extends Custom {
    private Fork fork;
    private Spoon spoon;
    private Knife knife;
    private DinnerPlate dinnerPlate;

    public PlaceSetting(int i) {
        super(i + 1);
        fork = new Fork(i + 2);
        spoon = new Spoon(i + 3);
        knife = new Knife(i + 4);
        dinnerPlate = new DinnerPlate(i + 5);
    }

    public static void main(String[] args) {
        PlaceSetting settings = new PlaceSetting(10);
    }
}
