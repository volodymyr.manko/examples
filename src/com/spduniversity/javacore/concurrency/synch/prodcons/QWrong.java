package com.spduniversity.javacore.concurrency.synch.prodcons;

public class QWrong implements Q {
    int packages;

    @Override
    public  int get() {
        System.out.println("Received: " + packages);

        return packages;
    }

    @Override
    public void put(int packages) {
        this.packages = packages;
        System.out.println("Sent: " + this.packages);
    }
}
