package com.spduniversity.javacore.concurrency.examples.executors;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class FixedThreadPoolShutDown {

    public static void main(String[] args) {

        ExecutorService executor = Executors.newFixedThreadPool(3);

        Runnable task = () -> {
            String threadName = Thread.currentThread().getName();

            // Modeling heavy processing
            try {
                TimeUnit.SECONDS.sleep(4);
                System.out.println("FINISHED");
            } catch (InterruptedException e) {
                System.out.println("Task1 was interrupted");
            }

            System.out.println("Task1 " + threadName);
        };

        executor.submit(task);
        executor.submit(task);
        executor.submit(task);

        try {
            System.out.println("attempt to shutdown executor");

            // Soft shuts down
            executor.shutdown();
            executor.awaitTermination(3, TimeUnit.SECONDS);
        }
        catch (InterruptedException e) {
            System.err.println("tasks interrupted");
        }
        finally {
            if (!executor.isTerminated()) {
                System.err.println("cancel non-finished tasks");
            }
            // Shut down all tasks forcibly
            executor.shutdownNow();
            System.out.println("shutdown finished");
        }
    }
}
