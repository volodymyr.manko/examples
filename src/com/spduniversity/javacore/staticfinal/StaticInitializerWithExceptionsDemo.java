package com.spduniversity.javacore.staticfinal;

public class StaticInitializerWithExceptionsDemo {
    static SomeStuff stuff;
    static SomeStuff anotherStuff = initStuff();

    static {
        try {
            stuff = getStuff();
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    private static SomeStuff getStuff() throws Exception {
        throw new Exception("Hi!");
    }

    private static SomeStuff initStuff() {
        try {
            return getStuff();
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return null;
    }

    public static void main(String[] args) {

    }
}
