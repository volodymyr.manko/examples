package com.spduniversity.javacore.abstractintefaces.pet.button;

@FunctionalInterface
public interface ICallback {
	void doSomething();
}
