package com.spduniversity.javacore.concurrencyadv.forkjoin.recusiveaction;

import java.util.concurrent.ForkJoinPool;

public class RecurActionDemo {
    public static void main(String[] args) {

        double[] nums = new double[100000];

        for (int i = 0; i < nums.length; i++) {
            nums[i] = (double) i;
        }

        System.out.println("Part of source sequence");
        for (int i = 0; i < 10; i++) {
            System.out.print(nums[i] + " ");
        }
        System.out.println();

        // Variant 1
//        ForkJoinPool pool = new ForkJoinPool();
//        SqrtTransformTask task = new SqrtTransformTask(nums, 0, nums.length);
//        pool.invoke(task);

        // Variant 2
        ForkJoinPool pool = ForkJoinPool.commonPool();
//        SqrtTransformTask task = new SqrtTransformTask(nums, 0, nums.length);
//        pool.invoke(task);

        // Variant 3
        SqrtTransformTask task = new SqrtTransformTask(nums, 0, nums.length);
        task.invoke();

        System.out.println("Results");
        for (int i = 0; i < 10; i++) {
            System.out.format("%.4f ", nums[i]);
        }
    }
}
