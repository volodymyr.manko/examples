package com.spduniversity.javacore.concurrency;

import java.util.concurrent.TimeUnit;

public class ThreadJava8Demo {

    public static void main(String[] args) throws InterruptedException {
        Runnable task = () -> {
            String threadName = Thread.currentThread().getName();
            System.out.println("Inside run()... " + threadName);
        };

        Thread thread = new Thread(task);
        thread.start();

        // More simple
        new Thread(() -> System.out.println("Another thread()... "
                + Thread.currentThread().getName())).start();

        System.out.println("Inside main() method...");
    }
}
