package com.spduniversity.javacore.concurrency.synch;

import java.util.concurrent.TimeUnit;

class Callme {

    void call(String msg) {
        System.out.print("[ " + msg);

        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println(" ]");
    }
}
